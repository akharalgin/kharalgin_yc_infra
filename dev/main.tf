resource "yandex_compute_instance" "vm-1" {
  
  platform_id = "standard-v2"
  zone = "ru-central1-a"
  name = "k8s-cp-01"
  hostname    = "k8s-cp-01.${var.STAGE}.${var.L2_DOMAIN}.${var.L1_DOMAIN}"
  allow_stopping_for_update = true

  resources {
    cores         = 2
    core_fraction = 5
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8b24tqvq7t2f8a1o1s"
      size     = 30
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.yc-tf-a.id
    nat       = true
  }

  metadata = {
#    "user-data": "#cloud-config\nusers:\n  - name: akharalgin\n    groups: sudo\n    shell: /bin/bash\n    sudo: 'ALL=(ALL) NOPASSWD:ALL'\n    ssh-authorized-keys:\n      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5lGdab00aVwu7lZWBCZEPohX2x7eHLOAlONYzqS7QRckg9I5m+jfNujF9lwmiynQJaXSepWTqb21SKvOVp/2U1hcf4fShvFHBR7LLBEOTMJ9JfhG1HsRgddd01vP7mmgd5a5TxWsbQ28yIxp58ul8GnEZrTMkjuh+bh6HphRm82/cjxh9E0U0c+3vqnjlAdpOa9BlqBMUDkax6x+WnFDBtbh+BEQJySREKtWsXzt/VKs729XA4boJhIAjzpLgFaaIzsukEhXn7oUKg3wcTlsqTdr+mr/dSd4hP3YpHKysut7+m/7YmaaYetoZ4lvprunr4mOr6YRcYwefVbZ0sd1L akharalgin@akharalgin-Latitude-3410"
    user-data = "${file("../meta_info.yaml")}"
  }
}

resource "yandex_compute_instance" "vm-2" {
  
  platform_id = "standard-v2"
  zone = "ru-central1-b"
  name = "k8s-wrk-01"
  hostname    = "k8s-wrk-01.${var.STAGE}.${var.L2_DOMAIN}.${var.L1_DOMAIN}"
  allow_stopping_for_update = true

  resources {
    cores         = 2
    core_fraction = 5
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8b24tqvq7t2f8a1o1s"
      size     = 30
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.yc-tf-b.id
    nat       = true
  }

  metadata = {
#    "user-data": "#cloud-config\nusers:\n  - name: akharalgin\n    groups: sudo\n    shell: /bin/bash\n    sudo: 'ALL=(ALL) NOPASSWD:ALL'\n    ssh-authorized-keys:\n      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5lGdab00aVwu7lZWBCZEPohX2x7eHLOAlONYzqS7QRckg9I5m+jfNujF9lwmiynQJaXSepWTqb21SKvOVp/2U1hcf4fShvFHBR7LLBEOTMJ9JfhG1HsRgddd01vP7mmgd5a5TxWsbQ28yIxp58ul8GnEZrTMkjuh+bh6HphRm82/cjxh9E0U0c+3vqnjlAdpOa9BlqBMUDkax6x+WnFDBtbh+BEQJySREKtWsXzt/VKs729XA4boJhIAjzpLgFaaIzsukEhXn7oUKg3wcTlsqTdr+mr/dSd4hP3YpHKysut7+m/7YmaaYetoZ4lvprunr4mOr6YRcYwefVbZ0sd1L akharalgin@akharalgin-Latitude-3410"
    user-data = "${file("../meta_info.yaml")}"
    }
}

resource "yandex_vpc_network" "yc-tf" {
  name = "k8s-net-${var.STAGE}"
}

resource "yandex_vpc_subnet" "yc-tf-a" {
  name           = "k8s-subnet-a"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.yc-tf.id
  v4_cidr_blocks = ["192.168.1.0/24"]
}

resource "yandex_vpc_subnet" "yc-tf-b" {
  name           = "k8s-subnet-b"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.yc-tf.id
  v4_cidr_blocks = ["192.168.2.0/24"]
}

resource "yandex_vpc_subnet" "yc-tf-c" {
  name           = "k8s-subnet-c"
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.yc-tf.id
  v4_cidr_blocks = ["192.168.3.0/24"]
}
resource "yandex_vpc_subnet" "yc-tf-d" {
  name           = "k8s-subnet-d"
  zone           = "ru-central1-d"
  network_id     = yandex_vpc_network.yc-tf.id
  v4_cidr_blocks = ["192.168.4.0/24"]
}

resource "yandex_dns_zone" "zone1" {
  name             = "${var.STAGE}-${var.L2_DOMAIN}-${var.L1_DOMAIN}"
  description      = "desc"
  labels = {
    label1 = "label-1-value"
  }
  zone             = "${var.STAGE}.${var.L2_DOMAIN}.${var.L1_DOMAIN}."
  public           = true
}

resource "yandex_dns_recordset" "dns-record-01" {
  zone_id = yandex_dns_zone.zone1.id
  name    = yandex_compute_instance.vm-1.name
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm-1.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "dns-record-02" {
  zone_id = yandex_dns_zone.zone1.id
  name    = yandex_compute_instance.vm-2.name
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm-2.network_interface.0.nat_ip_address]
}

output "Internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}
output "External_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

output "Internal_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.ip_address
}
output "External_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}