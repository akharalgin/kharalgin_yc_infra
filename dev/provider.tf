terraform {

  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "> 0.8"
    }
  }
  backend "http" {}
  required_version = ">= 0.13"
}
provider "yandex" {
#  token                    = "${var.YC_TF_SA_TOKEN}"
  service_account_key_file = "sa-key.json"
  cloud_id                 = "b1g0n5sfdfm3orvpfnh3"
  folder_id                = "b1gut0hvcn000qu0jub1"
}