variable YC_TF_SA_TOKEN {
    default = ""
}
variable YC_CLOUD_ID {
    default = ""
}
variable YC_FOLDER_ID {
    default = ""
}
variable STAGE {
    default = "dev"
}
variable L1_DOMAIN {
    default = "online"
}
variable L2_DOMAIN {
    default = "kharalgin"
}